# 5pm.dev Search Module

Small JavaScript library that uses the GitLab API to search static Hugo websites.

*We currently only support Gitlab, we do have plans to add support for Github in the future.*

## Implementation

Replace the ```{{ ... }}``` with the value needed to make search work properly.  
I have found the best way to get an open access token is to create a new account that doesn't have API access to any other repo than the one you are searching.

### Comment Style and Layout

Content comment block should be on every post you want to search.  
The current Version only supports content block comment of the following layout: ***anything missing or added will be ignored***.

```md
---
title: Post Title Here
subtitle: Post sub-title here
excerpt: Short post excerpt
image: URL to image if the post has one
tags: [Tag 1, Tag 2, Tag 3]
date: Published date here
author: Author here
---
```

### Include the script search script on the page

```html
<script src="{{ Path to search.js }}"></script>
```

### Initialize the 5pm Search module

```js
FivePM.Search.Init({
    API: 'GitLab',
    Token: '{{ Access Token }}',
    Project: '{{ Project ID }}',
    Directory: '/{{ content directory }}/',
    Container: '{{ ID for the container to put the search in }}'
});
```

### Usage

The script should automatically add a responsive search console into the container provided in the settings during initialization.
It has a search back, just tyoe your search query in and press enter, it will handle all the heavy lifting and display the first 10 results.
It should also paginate the results in sets of 10 to allow for deaper searches, up to 10 pages.

### Credit

Pure CSS Loading icon by https://icons8.com/cssload